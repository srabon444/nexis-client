# Nexis Task

An Inventory Management platform built with MERN stack


## [Live Application](https://delicate-heliotrope-9a85c4.netlify.app/)

![App Screenshot 1](n1.png)
![App Screenshot 2](n2.png)
![App Screenshot 3](n3.png)
![App Screenshot 4](n4.png)
![App Screenshot 5](n5.png)

## Tech Stack

**Fronted:** 
- React Js (create-react-app)
- Axios
- Material UI
- Material Icons
- Context Api
- Token Validation



**Hosting:**
- Render

## Features

- Fully functional Authentication (Login, Signup)
- Employee List with Details
- Employees 30days Attendance Details

  
## Run Locally

Clone the project

```bash
  git clone https://gitlab.com/srabon444/nexis-client.git
```

Go to the project directory

```bash
  cd nexis-client
```

Install dependencies

```bash
  npm install
```

  
## Sample login credentials:

```bash
srabon6598@gmail.com
12345678
```
